# Command and control
## Intro
Basic (sub)system description
## Scope
What is the main scope of this (sub)system
## Assumptions
What we will be doing, and not doing (focus on the not doing)
## Requirements
Specifications available free.
Highly modular and customised.
Lightweight (targeted for limited resources in a microcontroller)
Low protocol overhead.
## Components
 - core:
Software components that are used from e.g. memory management.
 - standard definition and implementation.
 - system protocol interface:
Interface with other systems in various buses (e.g UART, I2C).
 - templating standard.
 - integration extensions ( e.g. RTOS components).
Code for easily integrating various software.

## Related Systems
All systems that have software that interact with other systems and or the ground station.
## External Links

