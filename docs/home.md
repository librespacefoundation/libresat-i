# LibreSAT-i
This is the documentation for the open source hardware and software platform for cubesats by Libre Space Foundation, codename LibreSAT-i.

## Intro
[Libre Space Foundation](https://librespacefoundation.org) aspires to create a re-usable, open source hardware and software, cubesat platform, utilizing state of the art technologies and latest engineering practices. The vision is to create a vibrant engineering community that can design, develop, manufacture, test and deploy all needed technologies for a complete cubesat platform.

## Scope
The scope of LibreSAT-i is the complete design of a cubesat according to [published standards](http://cubesat.org).

## [Systems Design](systems-design.md)
In this page you can find a list of all different components of the general systems design.

## Contribute
TBD

## Communicate
Join us on [riot.im](https://riot.im/app/#/room/#librespace:matrix.org) to discuss about the development of LibreSAT-i.
