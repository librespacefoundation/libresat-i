# (sub)system Name
## Intro
Basic (sub)system description
## Scope
What is the main scope of this (sub)system
## Assumptions
What we will be doing, and not doing (focus on the not doing)
## Requirements
Detailed requirements with links and documentation-rationale
## Components
List of components of the subsystem. e.g.
* Software
 * control loop
 * reusable library x
* Hardware
 * blah-blah

## Related Systems
Links to related systems if needed
## External Links
List of links for further reading